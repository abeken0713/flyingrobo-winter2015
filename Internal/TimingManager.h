/*
 * TimingManager.h
 *
 *  Created on: 2014/12/31
 *      Author: 岩滝　宗一郎
 */

#ifndef TIMINGMANAGER_H_
#define TIMINGMANAGER_H_
/*! @addtogroup Internal*/
/*! @{*/
typedef void(*_loop_func)(void);
#ifdef __cplusplus
/**
 *
 * @brief 周期的な処理を実行させる
 */
class TimingManager {
public:
	enum TimingState{
			Request_DeviceUpdate,WAIT_Update,Exec_Loop,Device_Timeout,Sleeping
		};
private:
	static void PendSV_Request(void);
	static TimingState state;
	static int Period;
	static _loop_func PeriodicFunc;
	static int elapsed_time;
public:
	static void Exec(void);
	/**
	 * @brief 制御周期と実行する関数を指定して，周期処理を開始する．
	 * @param ControlPeriod 周期[ms]
	 * @param Func 実行すべき関数[void(*func)(void)]
	 */
	static void Init(int ControlPeriod,_loop_func Func);
	static int GetElapsedTIme(void);
	static void PendSV_Handler(void);
};
#endif
#ifdef __cplusplus
extern"C"{
#endif
void Timingmanager_Exec(void);
void Timingmanager_PendSV_Handler(void);
#ifdef __cplusplus
}
#endif
/*! @}*/
#endif /* TIMINGMANAGER_H_ */
