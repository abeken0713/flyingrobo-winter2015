/*
 * DeviceManager.h
 *
 *  Created on: 2014/12/30
 *      Author: 岩滝　宗一郎
 */

#ifndef DEVICEMANAGER_H_
#define DEVICEMANAGER_H_
#include "HAL/AbstractHardware.h"
/*! @addtogroup Internal*/
/*! @{*/
/**
 *
 * @brief 抽象デバイスの管理
 * 登録，更新などを行う
 */
class DeviceManager {
private:
	static const int _DeviceNumMAX=20;///<デバイスの最大数
	static AbstractHardware*_DeviceList[];
	static int _DeviceNum;
public:
	DeviceManager();
	/**
	 * @brief デバイスを追加する
	 * @param device
	 * @return　登録成功で1を返す
	 */
	static int Add(AbstractHardware*device);
	/**
	 * @brief 登録されているデバイスをすべて更新する．（更新要求を発行する）
	 */
	static void Update(void);
	/**
	 * @brief 更新されているか調べる．
	 * @return 更新完了=true
	 */
	static bool IsUpdated(void);
	virtual ~DeviceManager();
};
/*! @} */
#endif /* DEVICEMANAGER_H_ */
