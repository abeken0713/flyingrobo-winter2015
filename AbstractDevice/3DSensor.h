/*
 * Accelerometer3D.h
 *
 *  Created on: 2014/12/18
 *      Author: �@��Y
 */

#ifndef __3DSensor_H_
#define __3DSensor_H_
#include "LinearAlgebra/Vector.h"
class Sensor3D{
protected:
	float x;
	float y;
	float z;
	float GainX;
	float GainY;
	float GainZ;
	int OffsetX;
	int OffsetY;
	int OffsetZ;
public:
	Sensor3D();
	Sensor3D(float GainX,float GainY,float GainZ,int OffsetX,int OffsetY,int OffsetZ);
	void SetGain(float GainX,float GainY,float GainZ);
	void SetOffset(int OffsetX,int OffsetY,int OffsetZ);
	void SetRawValue(int x,int y,int z);
	void Get(float& x,float& y,float& z);
	Vector Get(void);
};


#endif /* ACCELEROMETER3D_H_ */
