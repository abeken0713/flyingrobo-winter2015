/*
 * SerialIO.h
 *
 *  Created on: 2015/08/28
 *      Author: �@��Y
 */

#ifndef SERIALIO_H_
#define SERIALIO_H_

#include "AbstractHardware.h"

class SerialIO: public AbstractHardware {
private:
	int fd;
	int max_buff_len;
	char*input_buff;
	int*refcount;
	int inputbuffIndex;
	bool recv_fin;
public:
	SerialIO();
	SerialIO(const SerialIO&origin);
	SerialIO(char*Name,int input_buf_length);
	char*GetReceivedString(void);
	void UpdateRequest(void);

	bool IsUpdateFinished(void);
	virtual ~SerialIO();
};

#endif /* SERIALIO_H_ */
