/*
 * FullcolorLED.cpp
 *
 *  Created on: 2014/12/30
 *      Author: ���@�@��Y
 */

#include "FullcolorLED.h"

FullcolorLED::FullcolorLED() {
	// TODO Auto-generated constructor stub

}
FullcolorLED::FullcolorLED(DigitalOut*red,DigitalOut*green,DigitalOut*blue,bool Polarity){
	this->B=blue;
	this->G=green;
	this->R=red;
	this->Polarity=Polarity;
	if(Polarity){
		this->B->Write(false);
		this->G->Write(false);
		this->R->Write(false);
	}else{
		this->B->Write(true);
		this->G->Write(true);
		this->R->Write(true);
	}
}
void FullcolorLED::Set(Color col){
	clr=col;
}
void FullcolorLED::UpdateRequest(void){
	if(clr&Red){
		this->R->Write(true==this->Polarity);
	}else{
		this->R->Write(false==this->Polarity);
	}
	if(clr&Green){
		this->G->Write(true==this->Polarity);
	}else{
		this->G->Write(false==this->Polarity);
	}
	if(clr&Blue){
		this->B->Write(true==this->Polarity);
	}else{
		this->B->Write(false==this->Polarity);
	}
}
bool FullcolorLED::IsUpdateFinished(){
	return true;
}
FullcolorLED::~FullcolorLED() {
	// TODO Auto-generated destructor stub
}

