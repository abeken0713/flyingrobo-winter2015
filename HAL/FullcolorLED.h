/*
 * FullcolorLED.h
 *
 *  Created on: 2014/12/30
 *      Author: 岩滝　宗一郎
 */

#ifndef FULLCOLORLED_H_
#define FULLCOLORLED_H_
#include "Peripheral/gpio.h"
#include "AbstractHardware.h"
/**
 * @ingroup HAL
 * @brief FullColorLEDクラス　フルカラーLEDを定義
 */
class FullcolorLED:public AbstractHardware {
public:
	/**
	 * @enum FullColorLEDの色を表す
	 */
	enum Color{Black=0,Red=0x1,Green=0x2,Blue=0x04,Yellow=0x03,Cyan=0x06,Magenta=0x05,White=0x07};
private:
	DigitalOut*R;
	DigitalOut*G;
	DigitalOut*B;
	bool Polarity;
	Color clr;
public:

	FullcolorLED();
	/**
	 * @brief FullColorLEDのコンストラクタ
	 * @param red 赤LEDの繋がっているDigitalOut へのポインタ
	 * @param green 緑LEDのつながっているDigitalOutへのポインタ
	 * @param blue 青LEDのつながっているDigitalOutへのポインタ
	 * @param Polarity ポートが'H'levelで点灯する場合trueを設定
	 * @sa DigitalOut
	 */
	FullcolorLED(DigitalOut*red,DigitalOut*green,DigitalOut*blue,bool Polarity);
	/**
	 *
	 * @param col 色を設定
	 *
	 */
	void Set(Color col);
	void UpdateRequest(void);
	bool IsUpdateFinished();
	void HardwareCB(uint16_t*buf,int size){

	}
	virtual ~FullcolorLED();
};

#endif /* FULLCOLORLED_H_ */
