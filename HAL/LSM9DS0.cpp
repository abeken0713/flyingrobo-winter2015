/*
 * LSM9DS0.cpp
 *
 *  Created on: 2014/12/28
 *      Author: 岩滝　宗一郎
 *      説明：9自由度センサモジュールLSM9DS0のドライバ
 */

#include "LSM9DS0.h"
#include <stdio.h>

LSM9DS0::LSM9DS0(SPI*port,DigitalOut* CSbitG,DigitalOut*CSbitXM){
	this->io=port;
	this->CS_G=CSbitG;
	this->CS_XM=CSbitXM;

	this->CS_G->Write(true);
	this->CS_XM->Write(true);
	printf("Trying Sensor init\n\r");
	this->io->AddHandeler((AbstractHardware*)this,0,static_cast<void(AbstractHardware::*)(uint16_t*,int)>(&LSM9DS0::HardwareCB));//static_cast<void(AbstractHardware::*)(uint16_t*,int)>
	this->io->Config(SPI::Mode3,SPI::Master,SPI::div16,SPI_DataSize_16b);
	this->init_ok=false;
	//this->CS_XM->Write(false);
	this->gyro_accessing=false;
	this->xm_accessing=false;
	this->state=Wait;
	printf("Handler registered\n\r");
	this->Read_reg_request(TgtXM,Who_AM_I_XM);
	while(this->xm_accessing);
	//return;
	if(retval_from_sensor==0x49){
		printf("XM WhoamI_OK\n\r");
	}else{
		printf("XM WhoamI_Err\n\r");
	}
	this->Read_reg_request(TgtGyro,Who_AM_I_G);
	while(this->gyro_accessing);
	if(retval_from_sensor==0xd4){
		printf("Gyro WhoamI OK\n\r");
	}else{
		printf("Gyro WhoamI Err\n\r");
	}
	Write_reg(TgtGyro,CTRL1_G,0x0f);
	//Write_reg(TgtGyro,CTRL2_G,0x28);
	while(this->gyro_accessing);
	Write_reg(TgtGyro,CTRL4_G,0x80|0x30);//Fullscale=2000deg/s
	while(this->gyro_accessing);
	Write_reg(TgtXM,CTRL1_XM,0x6F);
	while(this->xm_accessing);
	Write_reg(TgtXM,CTRL7_XM,0x00);
	while(this->xm_accessing);

}
void LSM9DS0::Write_reg(Target tgt,int addr,char data){
	if(tgt==TgtGyro){
		this->CS_XM->Write(true);
		this->CS_G->Write(false);
		this->gyro_accessing=true;
		this->xm_accessing=false;
	}else{
		this->CS_G->Write(true);
		this->CS_XM->Write(false);
		this->gyro_accessing=false;
		this->xm_accessing=true;
	}
	this->io->Write((addr<<8)|data);
}
void LSM9DS0::Read_reg_request(Target tgt,int addr){
	if(tgt==TgtGyro){
		//printf("TgtGyro\n\r");
		this->CS_XM->Write(true);
		this->CS_G->Write(false);
		this->gyro_accessing=true;
		this->xm_accessing=false;
	}else{
		//printf("TgtXM\n\r");
		this->CS_G->Write(true);
		this->CS_XM->Write(false);
		this->gyro_accessing=false;
		this->xm_accessing=true;
	}
	//printf("Send:%x",(addr<<8)|0x8000);
	this->io->Write((addr<<8)|0x8000);
}
void LSM9DS0::HardwareCB(uint16_t*buf,int size){
	//printf("Recv=%x\n\r",*buf);
	uint8_t data=(*buf)&0xff;
	retval_from_sensor=data;
	if(this->gyro_accessing){
		this->gyro_accessing=false;
		this->CS_G->Write(true);
	}
	if(this->xm_accessing){
		this->xm_accessing=false;
		this->CS_XM->Write(true);
	}
	switch(this->state){
	case Read_XLG:
		raw_gyro[0]=0;
		raw_gyro[0]|=data;
		this->state=Read_XHG;
		Read_reg_request(TgtGyro,OUT_X_H_G);
		break;
	case Read_XHG:
		raw_gyro[0]|=data<<8;
		this->state=Read_YLG;
		Read_reg_request(TgtGyro,OUT_Y_L_G);
		break;
	case Read_YLG:
		raw_gyro[1]=0;
		raw_gyro[1]|=data;
		this->state=Read_YHG;
		Read_reg_request(TgtGyro,OUT_Y_H_G);
		break;
	case Read_YHG:
		raw_gyro[1]|=data<<8;
		this->state=Read_ZLG;
		Read_reg_request(TgtGyro,OUT_Z_L_G);
		break;
	case Read_ZLG:
		raw_gyro[2]=0;
		raw_gyro[2]|=data;
		this->state=Read_ZHG;
		Read_reg_request(TgtGyro,OUT_Z_H_G);
		break;
	case Read_ZHG:
		raw_gyro[2]|=data<<8;
		this->state=Read_XLA;
		Read_reg_request(TgtXM,OUT_X_L_A);
		break;
	case Read_XLA:
		raw_acc[0]=0;
		raw_acc[0]|=data;
		this->state=Read_XHA;
		Read_reg_request(TgtXM,OUT_X_H_A);
		break;
	case Read_XHA:
		raw_acc[0]|=data<<8;
		this->state=Read_YLA;
		Read_reg_request(TgtXM,OUT_Y_L_A);
		break;
	case Read_YLA:
		raw_acc[1]=0;
		raw_acc[1]|=data;
		this->state=Read_YHA;
		Read_reg_request(TgtXM,OUT_Y_H_A);
		break;
	case Read_YHA:
		raw_acc[1]|=data<<8;
		this->state=Read_ZLA;
		Read_reg_request(TgtXM,OUT_Z_L_A);
		break;
	case Read_ZLA:
		raw_acc[2]=0;
		raw_acc[2]|=data;
		this->state=Read_ZHA;
		Read_reg_request(TgtXM,OUT_Z_H_A);
		break;
	case Read_ZHA:
		raw_acc[2]|=data<<8;
		this->state=Read_XLM;
		Read_reg_request(TgtXM,OUT_X_L_M);
		break;
	case Read_XLM:
		raw_mag[0]=0;
		raw_mag[0]|=data;
		this->state=Read_XHM;
		Read_reg_request(TgtXM,OUT_X_H_M);
		break;
	case Read_XHM:
		raw_mag[0]|=data<<8;
		this->state=Read_YLM;
		Read_reg_request(TgtXM,OUT_Y_L_M);
		break;
	case Read_YLM:
		raw_mag[1]=0;
		raw_mag[1]|=data;
		this->state=Read_YHM;
		Read_reg_request(TgtXM,OUT_Y_H_M);
		break;
	case Read_YHM:
		raw_mag[1]|=data<<8;
		this->state=Read_ZLM;
		Read_reg_request(TgtXM,OUT_Z_L_M);
		break;
	case Read_ZLM:
		raw_mag[2]=0;
		raw_mag[2]|=data;
		this->state=Read_ZHM;
		Read_reg_request(TgtXM,OUT_Z_H_M);
		break;
	case Read_ZHM:
		raw_mag[2]|=data<<8;
		this->update_finished=true;
		this->state=Wait;
		this->Accelerometer.SetRawValue(raw_acc[0],raw_acc[1],raw_acc[2]);
		this->Compass.SetRawValue(raw_mag[0],raw_mag[1],raw_mag[2]);
		this->Gyro.SetRawValue(raw_gyro[0],raw_gyro[1],raw_gyro[2]);
		break;
	default:
		break;
	}
}
void LSM9DS0::UpdateRequest(void){
	this->update_finished=false;
	this->Read_reg_request(TgtGyro,OUT_X_L_G);
	this->state=Read_XLG;
}
bool LSM9DS0::IsUpdateFinished(void){
	return this->update_finished;
}
void LSM9DS0::ReadRaw(int16_t*Gyro,int16_t*Acc,int16_t*Mag){
	int i=0;
	for(i=0;i<3;i++){
		*(Gyro+i)=raw_gyro[i];
		*(Acc+i)=raw_acc[i];
		*(Mag+i)=raw_mag[i];
	}
}
