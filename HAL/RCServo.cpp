/*
 * RCServo.cpp
 *
 *  Created on: 2014/12/30
 *      Author: ���@�@��Y
 */

#include "RCServo.h"

RCServo::RCServo(PWMOutputPin*pin,int Centerval,int Coeff){
	this->pin=pin;
	this->Centerval=Centerval;
	this->Coeff=Coeff;
}
void RCServo::Set(float deg){
	duty=Centerval+(deg*Coeff);
}
void RCServo::UpdateRequest(void){
	pin->Set(duty);
}
bool RCServo::IsUpdateFinished(void){
	return true;
}
void RCServo::SetParam(int Centerval,int Coeff){
	this->Centerval=Centerval;
	this->Coeff=Coeff;
}
