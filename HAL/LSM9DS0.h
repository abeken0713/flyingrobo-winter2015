/*
 * LSM9DS0.h
 *
 *  Created on: 2014/12/18
 *      Author: 宗一郎
 */

#ifndef LSM9DS0_H_
#define LSM9DS0_H_
#include "AbstractDevice/3DSensor.h"
#include "Peripheral/spi.h"
#include "Peripheral/gpio.h"
/**
 * @ingroup HAL
 * @brief ジャイロ加速度地磁気各3軸のセンサモジュールLSM9DS0
 */
class LSM9DS0:public AbstractHardware{
private:
	SPI*io;
	DigitalOut*CS_G;
	DigitalOut*CS_XM;
	volatile bool init_ok;
	volatile bool gyro_accessing;
	volatile bool xm_accessing;
	volatile char retval_from_sensor;
	enum Target{TgtGyro=0,TgtXM=1};
	void Write_reg(Target tgt,int addr,char data);
	void Read_reg_request(Target tgt,int addr);
	enum Register{Who_AM_I_G=0x0f,CTRL1_G=0x20,CTRL2_G=0x21,CTRL3_G=0x22,CTRL4_G=0x23,CTRL5_G=0x24,REF_G=0x25,STATUS_G=0x27
						,OUT_X_L_G=0x28,OUT_X_H_G=0x29,OUT_Y_L_G=0x2a,OUT_Y_H_G=0x2b,OUT_Z_L_G=0x2c,OUT_Z_H_G=0x2d
						,FIFOCTRL_G=0x2e,FIFOSRC_G=0x2f,STATUS_M=0x07,OUT_X_L_M=0x08,OUT_X_H_M=0x09,OUT_Y_L_M=0x0a,OUT_Y_H_M=0x0b
						,OUT_Z_L_M=0x0c,OUT_Z_H_M=0x0d,Who_AM_I_XM=0x0f,OFFSET_X_L_M=0x16,OFFSET_X_H_M=0x17,OFFSET_Y_L_M=0x18
						,OFFSET_Y_H_M=0x19,OFFSET_Z_L_M=0x1a,OFFSET_Z_H_M=0x1b,REFX=0x1c,REFY=0x1d,REFZ=0x1e,CTRL0_XM=0x1f
						,CTRL1_XM=0x20,CTRL2_XM=0x21,CTRL3_XM=0x22,CTRL4_XM=0x23,CTRL5_XM=0x24,CTRL6_XM=0x25,CTRL7_XM=0x26
						,STATUS_A=0x27,OUT_X_L_A=0x28,OUT_X_H_A=0x29,OUT_Y_L_A=0x2a,OUT_Y_H_A=0x2b,OUT_Z_L_A=0x2c,OUT_Z_H_A=0x2d
						,FIFOCTRL=0x2e,FIFOSRC=0x2f};
	enum UpdateState{Read_XLG,Read_XHG,Read_YLG,Read_YHG,Read_ZLG,Read_ZHG,Read_XLA,Read_XHA,Read_YLA,Read_YHA
					,Read_ZLA,Read_ZHA,Read_XLM,Read_XHM,Read_YLM,Read_YHM,Read_ZLM,Read_ZHM,Wait};
	volatile UpdateState state;
	volatile bool update_finished;
	volatile int16_t raw_gyro[3];
	volatile int16_t raw_acc[3];
	volatile int16_t raw_mag[3];
public:
	/**
	 * 抽象3軸加速度センサ
	 */
	Sensor3D Accelerometer;
	/**
	 * 抽象コンパスセンサ
	 */
	Sensor3D Compass;
	/**
	 * 抽象ジャイロセンサ
	 */
	Sensor3D Gyro;
	/**
	 * HALの割り込みハンドラ
	 */
	void HardwareCB(uint16_t*buf,int size);
	/**
	 * HALのアップデート要求
	 */
	void UpdateRequest(void);
	/**
	 * アップデートが完了しているか確認
	 * 返り値がtrueなら最新
	 */
	bool IsUpdateFinished(void);
	/**
	 * @brief LSM9DS0初期化
	 *
	 * @param port　SPIポート
	 * @param CSbitG DigitalOutポート
	 * @param CSbitXM
	 */
	LSM9DS0(SPI*port,DigitalOut* CSbitG,DigitalOut*CSbitXM);
	/**
	 * @brief センサーから生の値を読み出す.
	 * @param Gyro ジャイロの値が入る．int16_t[3]
	 * @param Acc	加速度の値が入る．int16_t[3]
	 * @param Mag	磁気の値が入る．int16_t[3]
	 *
	 */
	void ReadRaw(int16_t*Gyro,int16_t*Acc,int16_t*Mag);
};


#endif /* LSM9DS0_H_ */
