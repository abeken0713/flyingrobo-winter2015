/*
 * Matrix.cpp
 *
 *  Created on: 2015/01/09
 *      Author: 宗一郎
 */

#include "Matrix.h"
#include <stdio.h>
Matrix::Matrix(void){
	row=0;
	col=0;
	Value=0;//NullPtr
	refcount=new int;
	*refcount=0;
}
Matrix::Matrix(int row,int col) {
	// TODO Auto-generated constructor stub
	this->Value=new float*[row];
	int i;
	for(i=0;i<row;i++){
		this->Value[i]=new float[col];
	}
	this->row=row;
	this->col=col;
	refcount=new int;
	*refcount=1;
	//printf("constructor,this->ref=%d\n\r",*refcount);
}
Matrix::Matrix(const Matrix&origin){
	this->row=origin.row;
	this->col=origin.col;
	this->Value=origin.Value;
	this->refcount=origin.refcount;
	(*(this->refcount))++;
	//printf("constructor,this->ref=%d,right->ref=%d\n\r",*(this->refcount),*(origin.refcount));
}
float Matrix::At(int row,int col)const{
	return this->Value[row][col];
}
void Matrix::At(int row,int col,float val){
	this->Value[row][col]=val;
}
/**
 * 浅いコピー
 * this[i][j]=right[i][j]
 * return this
 */
Matrix& Matrix::operator=(const Matrix&right){
	if(*(this->refcount)>1){
		(*(this->refcount))--;//今までのValueを参照しなくなる．refcount を減らす．
	}else if(*(this->refcount)==1){//thisインスタンスしかデータを参照していない．メモリをまず開放する．それから浅いコピ
		int i;
		for(i=0;i<row;i++){
			delete[]Value[i];
		}
		delete[]Value;
		delete refcount;
	}//(else refcount==0) デフォルトコンストラクタによる．refcountだけはdelする
	else{
		delete refcount;
	}
	this->row=right.row;
	this->col=right.col;
	this->Value=right.Value;
	this->refcount=right.refcount;
	(*(this->refcount))++;
	//printf("op:=,this->ref=%d,right->ref=%d\n\r",*(this->refcount),*(right.refcount));
	return (*this);
}
/**
 * 深いコピー
 *
 */
void Matrix::CopyTo(Matrix&destination){
	int i,j;
	if(*(destination.refcount)>1){//Value はdestinationに加えほかの場所から参照されている．開放してはいけない．refcountを減らす．
		(*(destination.refcount))--;
	}else if(*(destination.refcount)==1){//参照しているのがdestinationだけのばあい，メモリを解放してからコピーする．
		for(i=0;i<destination.row;i++){
			delete[]destination.Value[i];
		}
		delete[]destination.Value;
		delete destination.refcount;
	}else{
		delete destination.refcount;
	}
	destination.Value=new float*[row];
	destination.row=this->row;
	destination.col=this->col;
	for(i=0;i<row;i++){
		destination.Value[i]=new float[col];
		for(j=0;j<col;j++){
			destination.Value[i][j]=this->Value[i][j];
		}
	}
	destination.refcount=new int;
	*(destination.refcount)=1;
}
/**
 * 行列同士の加算
 */
const Matrix Matrix::operator+(const Matrix&right)const{
	Matrix result(this->row,this->col);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			result.Value[i][j]=this->Value[i][j]+right.Value[i][j];
		}
	}
	return result;
}
/**
 *行列同士の減算
 */
const Matrix Matrix::operator-(const Matrix&right)const{
	Matrix result(this->row,this->col);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			result.Value[i][j]=this->Value[i][j]-right.Value[i][j];
		}
	}
	return result;
}
/**
 * 行列の掛算
 */
const Matrix Matrix::operator*(const Matrix&right)const{
	Matrix result(this->row,right.col);
	int i,j,k;
	for(i=0;i<this->row;i++){
		for(j=0;j<right.col;j++){
			result.Value[i][j]=0.0f;
			for(k=0;k<this->col;k++){
				result.Value[i][j]+=this->Value[i][k]*right.Value[k][j];
			}
		}
	}
	return result;
}
/**
 * 行列＊スカラー
 */
const Matrix Matrix::operator*(float scalar){
	Matrix result(row,col);
	int i,j;
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			result.Value[i][j]=this->Value[i][j]*scalar;
		}
	}
	return result;
}
const Vector Matrix::operator*(const Vector&x)const{
	Vector result(this->row);
	int i,j;
	float sum=0.0f;
	for(i=0;i<result.dim;i++){
		sum=0.0f;
		for(j=0;j<x.dim;j++){
			sum+=this->Value[i][j]*x.Value[j];
		}
		result.Value[i]=sum;
	}
	return result;
}
/**
 * 転置
 */
const Matrix Matrix::Transposed(void)const{
	Matrix result(this->col,this->row);
	int i,j;
	for(i=0;i<result.row;i++){
		for(j=0;j<result.col;j++){
			result.Value[i][j]=this->Value[j][i];
		}
	}
	return result;
}
void Matrix::Transpose(void){

}
void Matrix::Print(void)const{
	int i,j;
	for(i=0;i<this->row;i++){
		for(j=0;j<this->col;j++){
			printf("%f ",this->Value[i][j]);
		}
		printf(";\n\r");
	}
}
Matrix::~Matrix() {
	// TODO Auto-generated destructor stub
	if(*refcount==0){//refcountが0 デフォルトコンストラクタで生成のちすぐ寿命が来た．Valueはnewされていないのでdeleteしてはいけない．
		delete refcount;
	//	printf("destructor,refcount=0\n\r");
		return;
	}
	//printf("destructor,decrement refcount\n\r");
	*refcount=*refcount-1;
	int i,j;
	if(*refcount==0){
	//	printf("Val:delete\n\r");
		for(i=0;i<row;i++){
			delete[] this->Value[i];
		}
		delete[]this->Value;
		delete refcount;
	}
}
const Matrix operator*(float scalar,const Matrix&right){
	Matrix result(right.row,right.col);
	int i,j;
	for(i=0;i<right.row;i++){
		for(j=0;j<right.col;j++){
			result.Value[i][j]=scalar*right.Value[i][j];
		}
	}
	return result;
}
