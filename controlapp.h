/*
 * controlapp.h
 *
 *  Created on: 2015/01/03
 *      Author: 岩滝　宗一郎
 */

#ifndef CONTROLAPP_H_
#define CONTROLAPP_H_
#include "Controller/PIDController.h"
#include "Controller/LampSignalGenerator.h"
#include "Controller/StateMachine.h"
#include "Controller/ComplementaryFlter.h"
#include "LinearAlgebra/Matrix.h"
#include "LinearAlgebra/Vector.h"
#include "LinearAlgebra/Quaternion.h"
#include <math.h>
#define RC_pulse_min 1000//パルス最小値[us]
#define RC_pulse_max 2000//パルス最大値[us]
#define RC_pulse_center 1500//パルス中央値[us]
#define STICK_DEADBAND 0.01f
#define RC_pulse_RockingWingThr 1600//ロッキングウイング開始閾値
#define RockingwingWait1 0.5f//動作開始時間[sec]
#define RockingwingAmplitude 1.4f//ロッキングウィング振幅[rad]
#define RockingwingPeriod 1.0f//ロッキングウィング加速完了時間[sec]
#define RockingwingWait2 1.05f//左移動完了時間[sec]
#define RockingwingPeriod2 2.25f//右への加速完了時間[sec]
#define RockingwingAmplitude2 1.4f//振幅その２[rad]
#define RockingwingWait3 2.2f//動作完了
#define RockingwingCycle 2//なん周期動作させるか
//! 使用する制御系プロウラムの部品
extern PIDController*YawOmegaPID;//Yaw角速度制御
extern PIDController*RollOmegaPID;//ロール角速度制御器
extern PIDController*PitchOmegaPID;//ピッチ角速度制御器
extern PIDController*PitchAnglePID;//ピッチ角姿勢制御
extern PIDController*RollAnglePID;//ロール角姿勢制御
extern LampSignalGenerator*PitchRefGen;
extern LampSignalGenerator*RollRefGen;
extern StateMachine*OperationStateMachine;
extern ComplementaryFlter<Vector>*AttitudeEstimationFilter;
extern Quaternion*Attitude;
void ControlInit(void);
void ControlExec(void);

void OutputDistributionToRotor(float thr,float roll,float pitch,float yaw,float*rotorFR,float*rotorFL,float*rotorRR,float*rotorRL);
void ParseExternalRC(SerialIO*RCtoSerial,int&ch1,int&ch2);

#endif /* CONTROLAPP_H_ */
