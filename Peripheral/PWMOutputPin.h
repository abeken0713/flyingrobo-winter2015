/*
 * PWMOutputPin.h
 *
 *  Created on: 2014/12/30
 *      Author: ���@�@��Y
 */

#ifndef PWMOUTPUTPIN_H_
#define PWMOUTPUTPIN_H_
#include <stm32f4xx_conf.h>
/*! @addtogroup Peripheral */
/*! @{*/
/**
 *
 * @brief PWM output class
 *
 */
class PWMOutputPin {
private:
	TIM_TypeDef*timer;
	int ch;
	int period; //!< [us]
public:
	/**
	 * @brief PWM output initialize
	 * @param Port GPIOx
	 * @param GPIO_Pin_x
	 * @param Timer TIMx
	 * @param OC_ch [1:4]
	 * @param Period [us]
	 * @param TOP [count]
	 */
	PWMOutputPin(GPIO_TypeDef*Port,int GPIO_Pin_x,TIM_TypeDef*Timer,int OC_ch,int Period,bool OpenCollector=false);
	/**
	 * @brief Set Duty
	 * @param pulse width[us]
	 */
	void Set(int pulse_width);
};
/* @}*/
#endif /* PWMOUTPUTPIN_H_ */
