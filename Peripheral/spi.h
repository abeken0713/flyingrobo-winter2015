/*
 * spi.h
 *
 *  Created on: 2014/12/10
 *      Author: �@��Y
 */

#ifndef SPI_H_
#define SPI_H_

#include <stm32f4xx_conf.h>
#include "HAL/AbstractHardware.h"
/*! @addtogroup Peripheral*/
/*! @{*/
typedef void(AbstractHardware::*HardwareCB)(uint16_t*buf,int size);
/**
 *
 * @brief SPI serial comm
 */
class SPI{
private:
	SPI_TypeDef*SPIPORT;
	int SPI_ch;
	GPIO_TypeDef*GPIOFORSPI;
	int SCKbit;
	int MISObit;
	int MOSIbit;
	AbstractHardware*reciever;
	HardwareCB TxfinCB;
	HardwareCB RXfinCB;
	static bool SPI1_busy;
	static bool SPI2_busy;
	static bool SPI3_busy;
public:
	enum SPI_MODE{Mode0=0x0,Mode1=0x01,Mode2=0x02,Mode3=0x03};
	enum SPI_DIR{Master=SPI_Mode_Master,Slave=SPI_Mode_Slave};
	enum SPI_CLKDIV{div2=0x0,div4=0x08,div8=0x10,div16=0x18,div32=0x20,div64=0x28,div128=0x30,div256=0x38};
	/**
	 * @brief SPI init
	 * @param ch
	 * @param GPIO_SPI
	 * @param MISO
	 * @param MOSI
	 * @param SCK
	 */
	SPI(int ch,GPIO_TypeDef*GPIO_SPI,int MISO,int MOSI,int SCK);
	/**
	 * @brief SPI configuration
	 *
	 * Essential to use SPI
	 * @param mode
	 * @param dir
	 * @param div
	 * @param SPI_Datasize_Bitwidth
	 */
	void Config(SPI_MODE mode,SPI_DIR dir,SPI_CLKDIV div,int SPI_Datasize_Bitwidth);
	/**
	 *
	 * @param reciever
	 * @param TxfinishCB
	 * @param RxfinishCB
	 */
	void AddHandeler(AbstractHardware*reciever,HardwareCB TxfinishCB,HardwareCB RxfinishCB);
	/**
	 *
	 * @param data
	 */
	void Writebyte(unsigned char data);
	/**
	 *
	 * @param data
	 */
	void Write(uint16_t data);
	void IRQ(void);
	int Begin();
	void End();
};
/*! @}*/
#endif /* SPI_H_ */
