/*
 * controlapp.cpp
 *
 *  Created on: 2015/01/03
 *      Author: 岩滝　宗一郎
 */
/**
 *  @addtogroup mini-AHRS
 *  @{
 */
#include "HardwareConf.h"
#include "controlapp.h"
#include "Internal/TimingManager.h"
#include <stdio.h>
#include "main.h"
#include "Controller/MadgwickFilter.h"

PIDController*YawOmegaPID;
PIDController*RollOmegaPID;
PIDController*PitchOmegaPID;
PIDController*PitchAnglePID;//ピッチ角姿勢制御
PIDController*RollAnglePID;//ロール角姿勢制御
LampSignalGenerator*PitchRefGen;
LampSignalGenerator*RollRefGen;
StateMachine*OperationStateMachine;
ComplementaryFlter<Vector>*AttitudeEstimationFilter;
Quaternion*Attitude;
MadgwickFilter*AttitudeMadgwick;
extern FILE*Xbeefile;
float roll_target=0.0f;
float pitch_target=0.0f;
/**
 *
 * @brief 制御アプリケーションのステート
 */
enum ControllerState{
	Initialize=0,//!<Init
	DisARM=1,
	ArmedOmega=2,
	ArmedAngle=3,
	RockingWing=4
};
int InitializeState(int pre_state){
	static int MeanCycyle=10;
	static int Idletime=1000;
	static int16_t gyro_sum[3]={0,0,0};
	int16_t gyro[3];
	int16_t dummy[3];
	static int32_t acc_sum[3]={0,0,0};
	int16_t acc[3];
	FCIND->Set(FullcolorLED::Red);
	if(Idletime>0){
		Idletime--;
		return Initialize;
	}
	if(MeanCycyle>0){
		Sensor9Dof->ReadRaw(gyro,acc,dummy);
		int i;
		for(i=0;i<3;i++){
			gyro_sum[i]+=gyro[i];
			acc_sum[i]+=acc[i];
		}
		MeanCycyle--;
		return Initialize;
	}else{
		Sensor9Dof->Gyro.SetOffset(gyro_sum[0]/10,gyro_sum[1]/10,gyro_sum[2]/10);
		Sensor9Dof->Accelerometer.SetOffset(acc_sum[0]/10,acc_sum[1]/10,0);
		float gravity=acc_sum[2]/10.0f;
		Sensor9Dof->Accelerometer.SetGain(1.0f/gravity,1.0f/gravity,1.0f/gravity);
	}
	return DisARM;
}
int DisARMState(int pre_state){
	static int ArmWait=60;
	FCIND->Set(FullcolorLED::Yellow);
	if((!Receiver_CH1->IsValid())||(!Receiver_CH2->IsValid())||(!Receiver_CH3->IsValid())||(!Receiver_CH4->IsValid())){
		return DisARM;
	}
	if(Receiver_CH3->Get()<RC_pulse_min*1.05f&&Receiver_CH4->Get()<RC_pulse_min*1.05f){
		ArmWait--;
		if(ArmWait==0){
			ArmWait=60;
			return ArmedOmega;
		}
	}else if(Receiver_CH3->Get()<RC_pulse_min*1.05f&&Receiver_CH4->Get()>RC_pulse_max*0.95f){
		ArmWait--;
		if(ArmWait==0){
			ArmWait=60;
			return ArmedAngle;
		}
	}else{
		ArmWait=60;

	}
	return DisARM;
}
int ArmedOmegaState(int prevState){
	static int Wait=60;
	static bool rockingwing_went=false;
	FCIND->Set(FullcolorLED::Green);
	if(Receiver_CH3->Get()<RC_pulse_min*1.05f&&Receiver_CH4->Get()<RC_pulse_min*1.05){
		Wait--;
		if(Wait==0){
			Wait=60;
			return ArmedAngle;
		}
	}else if(Receiver_CH3->Get()<RC_pulse_min*1.05f&&Receiver_CH4->Get()>RC_pulse_max*0.95f){
		Wait--;
		if(Wait==0){
			Wait=60;
			return DisARM;
		}
	}else{
		Wait=60;
	}
	int ExtRC1,ExtRC2;
	ParseExternalRC(ReceiverExt,ExtRC1,ExtRC2);
	if(ExtRC1>RC_pulse_RockingWingThr&&rockingwing_went==false){
		rockingwing_went=true;
		return RockingWing;
	}else{
		if(ExtRC1<=RC_pulse_RockingWingThr)
		rockingwing_went=false;
	}
	return ArmedOmega;
}
int ArmedAngleState(int prevState){
	static int Wait=60;
	static bool rockingwing_went=false;
	FCIND->Set(FullcolorLED::Blue);
	if(Receiver_CH3->Get()<RC_pulse_min*1.05f&&Receiver_CH4->Get()<RC_pulse_min*1.05){
		Wait--;
		if(Wait==0){
			Wait=60;
			return DisARM;
		}
	}else if(Receiver_CH3->Get()<RC_pulse_min*1.05f&&Receiver_CH4->Get()>RC_pulse_max*0.95){
		Wait--;
		if(Wait==0){
			Wait=60;
			return ArmedOmega;
		}
	}else{
		Wait=60;
	}
	int ExtRC1,ExtRC2;
	ParseExternalRC(ReceiverExt,ExtRC1,ExtRC2);
	if(ExtRC1>RC_pulse_RockingWingThr&&rockingwing_went==false){
		rockingwing_went=true;
		return RockingWing;
	}else{
		if(ExtRC1<=RC_pulse_RockingWingThr)
		rockingwing_went=false;
	}
	return ArmedAngle;

}
int RockingWingState(int Prevstate){
	static int WhereComeFrom;
	static int count=0;
	FCIND->Set(FullcolorLED::White);
	if(Prevstate==ArmedAngle||Prevstate==ArmedOmega){
		WhereComeFrom=Prevstate;
		count=0;
	}

	pitch_target=0.0f;
	if(count*0.005f<RockingwingWait1){
		roll_target=0.0f;
	}else if(count*0.005f<RockingwingPeriod){
		roll_target=RockingwingAmplitude*sinf(3.14f*(count*0.005f-RockingwingWait1)/(RockingwingPeriod-RockingwingWait1));
	}else if(count*0.005f<RockingwingWait2){
		roll_target=0.0f;
	}else if(count*0.005f<RockingwingPeriod2){
		roll_target=-RockingwingAmplitude*sinf(3.14f*(count*0.005f-RockingwingWait2)/(RockingwingPeriod2-RockingwingWait2));
	}else if(count*0.005f<RockingwingWait3){
		roll_target=0.0f;
	}
	int ExtRC1,ExtRC2;
	ParseExternalRC(ReceiverExt,ExtRC1,ExtRC2);
	if((ExtRC1<=RC_pulse_RockingWingThr)||((float)count*0.005f>=(float)RockingwingWait3)){
		count=0;
		return ArmedAngle;
	}
	count++;
	return RockingWing;
}
//! 制御　初期化
void ControlInit(void){
	//!ゲインセット
	Sensor9Dof->Accelerometer.SetGain(1.0f,1.0f,1.0f);
	Sensor9Dof->Accelerometer.SetOffset(0,0,0);
	Sensor9Dof->Compass.SetGain(1.0f,1.0f,1.0f);
	Sensor9Dof->Compass.SetOffset(0,0,0);
	Sensor9Dof->Gyro.SetGain(2000.0f/32768.0f*3.14f/180.0f,2000.0f/32768.0f*3.14f/180.0f,2000.0f/32768.0f*3.14f/180.0f);
	Sensor9Dof->Gyro.SetOffset(450,50,50);
	printf("Control Init:Gain set\n\r");
	//!制御器の初期化
	RollRefGen=new LampSignalGenerator();//!>目標姿勢発生
	YawOmegaPID=new PIDController(0.025f,0.001f,0,100.0f,1.0f,true);//!>ヨー制御器
	PitchRefGen=new LampSignalGenerator();//!>ピッチ目標姿勢発生
	RollOmegaPID=new PIDController(0.014f,0.0002f,0.0001f,100.0f,1.0,true);//!>ロール制御PIDController(0.009f,0.0001f,0.00005f,100.0f,1.0,true)PIDController(0.014f,0.0002f,0.0001f,100.0f,1.0,true)
	PitchOmegaPID=new PIDController(0.012f,0.0002f,0.0001f,100.0f,1.0,true);//!>ピッチ制御PIDController(0.012f,0.0002f,0.0001f,100.0f,1.0,true)PIDController(0.01f,0.0001f,0.00005f,100.0f,1.0,true)
	RollAnglePID=new PIDController(0.5f,0.0f,0.001f,100.0f,2000.0/180.0*3.14,true);//!>ロール姿勢角
	PitchAnglePID=new PIDController(0.5f,0.0f,0.001f,100.0f,2000.0/180.0*3.14,true);//!>ピッチ姿勢角
	AttitudeEstimationFilter=new ComplementaryFlter<Vector>(0.9995f);
	Attitude=new Quaternion(1.0f,0,0,0);
	AttitudeEstimationFilter->Initialize(Attitude->ToVector4d());
	AttitudeMadgwick=new MadgwickFilter(0.005f,0.001f);
	printf("Control Init:Controller init\n\r");
	OperationStateMachine=new StateMachine();
	//! ステートの登録
	OperationStateMachine->AddState(InitializeState);
	OperationStateMachine->AddState(DisARMState);
	OperationStateMachine->AddState(ArmedOmegaState);
	OperationStateMachine->AddState(ArmedAngleState);
	OperationStateMachine->AddState(RockingWingState);
	OperationStateMachine->Reset();
	printf("Control Init:State init\n\r");
}
float LimitRCPulseRange(float val){
	if(val>RC_pulse_max||val<RC_pulse_min){
		return RC_pulse_center;
	}
	return val;
}
bool RCPulseLimitDetect(float val){
	if(val>=RC_pulse_max||val<RC_pulse_min){
		return false;
	}
	return true;
}
float ApplyDeadBand(float val){
	float retval=0.0f;
	if(val<STICK_DEADBAND&&val>-STICK_DEADBAND){//abs(val)<stick deadband
		retval=0.0f;
	}else if(val>=STICK_DEADBAND){
		retval=(val-STICK_DEADBAND)/(1-STICK_DEADBAND);
	}else{//val<=Stick deadband
		retval=(val+STICK_DEADBAND)/(1-STICK_DEADBAND);
	}
	return retval;
}
//! 制御　実行
void ControlExec(void){
	OperationStateMachine->Execute();
	//!加速度　地磁気　角速度を取得
	Vector acc=Sensor9Dof->Accelerometer.Get();
	//Vector mag=Sensor9Dof->Compass.Get();
	Quaternion x_axis(0.0f,1.0f,0.0f,0.0f);
	Vector mag=(Attitude->Conj()*x_axis*(*Attitude)).Tovector3d();
	//mag.At(0,1.0f);mag.At(1,0.0f);mag.At(2,0.0f);
	Vector omega=Sensor9Dof->Gyro.Get();
//#define __Complemen 0
#ifdef __Complemen
	//!クオータニオン更新用に角速度の情報を含む4x4行列をつくる．
	Matrix Omegamat=Quaternion::FindOmegaMatrix(omega*dt);
	//!補正用に地磁気と加速度から回転行列を計算する．
	acc.Normalize();
	//mag.Normalize();
	Vector y_unitvector=acc^mag;
	y_unitvector.Normalize();
	Vector x_unitvector=y_unitvector^acc;
	x_unitvector.Normalize();
	Matrix R_correction(3,3);
	R_correction.At(0,0,x_unitvector.At(0));R_correction.At(0,1,y_unitvector.At(0));R_correction.At(0,2,acc.At(0));
	R_correction.At(1,0,x_unitvector.At(1));R_correction.At(1,1,y_unitvector.At(1));R_correction.At(1,2,acc.At(1));
	R_correction.At(2,0,x_unitvector.At(2));R_correction.At(2,1,y_unitvector.At(2));R_correction.At(2,2,acc.At(2));
	//!姿勢補正用クオータニオン
	Quaternion AttitudeCorrection(R_correction);
	//!相補フィルタを適用
	Vector AttitudeCorrectionVec=AttitudeCorrection.ToVector4d();//地磁気加速度から求めた姿勢
	Vector AttitudeFromGyroVec=Omegamat*Attitude->ToVector4d()*0.5f+Attitude->ToVector4d();//ジャイロから求めた新しい姿勢
	if(AttitudeCorrectionVec*AttitudeFromGyroVec<0.0f){
		AttitudeCorrectionVec=AttitudeCorrectionVec*(-1.0f);
	}
	Vector NewAttitude =AttitudeEstimationFilter->Execute(Omegamat*Attitude->ToVector4d(),AttitudeCorrectionVec);
	//!クオータニオンのノルムを1にする.
	NewAttitude.Normalize();
	AttitudeEstimationFilter->SaveState(NewAttitude);
	Attitude->FromVector(NewAttitude);
#else
	//Madgwick filter を用いて補正を行う
	AttitudeMadgwick->Update(omega,acc);
	*Attitude=AttitudeMadgwick->GetAttitude();

#endif
	//!傾きを求める
	Quaternion z_unit_world_q(0,0,0,1.0f);
	Quaternion z_unit_sensor_q=Attitude->Conj()*z_unit_world_q*(*Attitude);
	float ax,ay,az;
	ax=z_unit_sensor_q.Tovector3d().At(0);
	ay=z_unit_sensor_q.Tovector3d().At(1);
	az=z_unit_sensor_q.Tovector3d().At(2);
	float pitch;
	float roll;
	if(ay!=0.0f||az!=0.0f){
		roll=atanf(-ax/sqrt(ay*ay+az*az));
	}else{
		roll=3.14f/2.0f;
	}

	if(az!=0.0f){
		pitch=atanf(ay/az);
	}else{
		pitch=3.14f/2.0f;
	}
	static float rollRefPrev=0.0f;
	static float pitchRefPrev=0.0f;
	static float yawRefPrev=0.0f;
	static float thrPrev=0.0f;
	float rollRefRaw=Receiver_CH1->Get();
	float pitchRefRaw=Receiver_CH2->Get();
	float yawRefRaw=Receiver_CH4->Get();
	float thrRaw=Receiver_CH3->Get();
	if(!RCPulseLimitDetect(rollRefRaw)){
		rollRefRaw=rollRefPrev;
	}else{
		rollRefPrev=rollRefRaw;
	}
	if(!RCPulseLimitDetect(pitchRefRaw)){
		pitchRefRaw=pitchRefPrev;
	}else{
		pitchRefPrev=pitchRefRaw;
	}
	if(!RCPulseLimitDetect(yawRefRaw)){
		yawRefRaw=yawRefPrev;
	}else{
		yawRefPrev=yawRefRaw;
	}
	if(!RCPulseLimitDetect(thrRaw)){
		thrRaw=thrPrev;
	}else{
		thrPrev=thrRaw;
	}
	float rollRef=(rollRefRaw-RC_pulse_center)*1.0f/(RC_pulse_max-RC_pulse_center);//ロール目標[-1;1]
	float pitchRef=(pitchRefRaw-RC_pulse_center)*1.0f/(RC_pulse_max-RC_pulse_center);//ピッチ目標
	float yawRef=(yawRefRaw-RC_pulse_center)*1.0f/(RC_pulse_max-RC_pulse_center);//ヨー目標
	float thr=(thrRaw-RC_pulse_min)*1.0f/(RC_pulse_max-RC_pulse_min);//スロットル[0;1]
	float rollOmegaRef,pitchOmegaRef;
	float rollOutput=0.0f;
	float pitchOutput=0.0f;
	float yawOutput=0.0f;
	float rotorRR,rotorRL,rotorFR,rotorFL;
	rollRef=ApplyDeadBand(rollRef);
	pitchRef=ApplyDeadBand(pitchRef);
	yawRef=ApplyDeadBand(yawRef);
	if(OperationStateMachine->CheckCondition()==RockingWing){
		rollRef=roll_target;
		pitchRef=pitch_target;
	}
	if((OperationStateMachine->CheckCondition()==RockingWing||OperationStateMachine->CheckCondition()==ArmedAngle||OperationStateMachine->CheckCondition()==ArmedOmega)&&thr>0.1f){
		if(OperationStateMachine->CheckCondition()==RockingWing||OperationStateMachine->CheckCondition()==ArmedAngle){
			rollOmegaRef=RollAnglePID->Execute(rollRef-roll);
			pitchOmegaRef=PitchAnglePID->Execute(pitchRef-pitch);
			rollOutput=RollOmegaPID->Execute(rollOmegaRef-omega.At(1));
			pitchOutput=PitchOmegaPID->Execute(pitchOmegaRef-omega.At(0));
		}else{
			//<!PID実行
			rollOutput=RollOmegaPID->Execute(rollRef-omega.At(1));
			pitchOutput=PitchOmegaPID->Execute(pitchRef-omega.At(0));
		}
		yawOutput=YawOmegaPID->Execute(yawRef-omega.At(2));
		OutputDistributionToRotor(thr,rollOutput,pitchOutput,yawOutput,&rotorFR,&rotorFL,&rotorRR,&rotorRL);//出力割り当て

	}else{
		rotorFL=0;
		rotorFR=0;
		rotorRL=0;
		rotorRR=0;
		RollOmegaPID->Reset();
		PitchOmegaPID->Reset();
		YawOmegaPID->Reset();
		RollAnglePID->Reset();
		PitchAnglePID->Reset();
	}
	//servo1->Set(rotorFL);servo2->Set(rotorFR);//出力セット
	servo1->Set(rotorRL);servo2->Set(rotorFR);
	//servo3->Set(rotorRL);servo4->Set(rotorRR);
	servo3->Set(rotorFL);servo4->Set(rotorRR);
	static int screen_refresh=0;
	if(screen_refresh==10){//50msごとに再描画する．一般のPCはフレームレート60Hzのため，100Hzで画面クリアー＞描画を行うとうまく表示できない
		printf("\033[H");
		printf("\x1b[2J");
		printf("\x1b[?25l");
		printf("Gyro:\n\r");
		printf("X:%10d,Y:%10d,Z:%10d\n\r",(int)(omega.At(0)*1000.0f),(int)(omega.At(1)*1000.0f),(int)(omega.At(2)*1000.0f));
		printf("Acc:\n\r");
		printf("X:%10d,Y:%10d,Z:%10d\n\r",(int)(acc.At(0)*1000.0f),(int)(acc.At(1)*1000.0f),(int)(acc.At(2)*1000.0f));
		//printf("Mag:\n\r");
		//printf("X:%10d,Y:%10d,Z:%10d\n\r",(int)(mag.At(0)*1000.0f),(int)(mag.At(1)*1000.0f),(int)(mag.At(2)*1000.0f));
		//printf("x_unitv:\n\r");
		//printf("X:%10d,Y:%10d,Z:%10d\n\r",(int)(x_unitvector.At(0)*1000.0f),(int)(x_unitvector.At(1)*1000.0f),(int)(x_unitvector.At(2)*1000.0f));
		//printf("y_unitv:\n\r");
		//printf("X:%10d,Y:%10d,Z:%10d\n\r",(int)(y_unitvector.At(0)*1000.0f),(int)(y_unitvector.At(1)*1000.0f),(int)(y_unitvector.At(2)*1000.0f));
		//Vector Attc=AttitudeCorrection.ToVector4d();
		//printf("Attitude:\n\r");
		//printf("q0:%10d,q1:%10d,q2:%10d,q3:%10d\n\r",(int)(Attc.At(0)*1000.0f),(int)(Attc.At(1)*1000.0f),(int)(Attc.At(2)*1000.0f),(int)(Attc.At(3)*1000.0f));
		printf("Attitude\n\r");
		printf("q0:%10d,q1:%10d,q2:%10d,q3:%10d\n\r",(int)(Attitude->GetElem(0)*1000.0f),(int)(Attitude->GetElem(1)*1000.0f),(int)(Attitude->GetElem(2)*1000.0f),(int)(Attitude->GetElem(3)*1000.0f));
		printf("Z-axis:\n\r");
		printf("x=%10d,y=%10d,z=%10d\n\r",(int)(ax*1000.0f),(int)(ay*1000.0f),(int)(az*1000.0f));
		printf("Roll:%5d, Pitch:%5d\n\r",(int)(roll*1000.0f),(int)(pitch*1000.0f));
		printf("OmegaRef:%5d,%5d\n\r",(int)(rollOmegaRef*1000.0f),(int)(pitchOmegaRef*1000.0f));
		printf("Thr:%5d Roll:%5d Pitch:%5d Yaw:%5d\n\r",(int)(thr*1000),(int)(rollRef*1000),(int)(pitchRef*1000),(int)(yawRef*1000));
		printf("Ch1:%d,Ch2:%d,Ch3:%d,Ch4:%d\n\r",(int)Receiver_CH1->Get(),(int)Receiver_CH2->Get(),(int)Receiver_CH3->Get(),(int)Receiver_CH4->Get());
		printf("FL:%5d FR:%5d\n\r",(int)(rotorFL*1000.0f),(int)(rotorFR*1000.0f));
		printf("RL:%5d RR:%5d\n\r",(int)(rotorRL*1000.0f),(int)(rotorRR*1000.0f));
		printf("State:%d\n\r",OperationStateMachine->CheckCondition());
		printf("Elapsed time%d[ms]\n\r",TimingManager::GetElapsedTIme());
		screen_refresh=0;
	}
	//screen_refresh++;

	fprintf(Xbeefile,"%d,%d,%d,%d,%d,%d,%d,%d,%d\n\r",OperationStateMachine->CheckCondition(),(int)(roll*1000.0f),(int)(pitch*1000.0f),(int)(rollRef*1000.0f),(int)(pitchRef*1000.0),(int)(rotorFL*1000.0f),(int)(rotorFR*1000.0f),(int)(rotorRL*1000.0f),(int)(rotorRR*1000.0f));
	//fprintf(Xbeefile,",%d,%d\n\r",(int)(Receiver_CH1->Get()),(int)(Receiver_CH2->Get()));
}
//出力割り当て関数
void OutputDistributionToRotor(float thr,float roll,float pitch,float yaw,float*rotorFR,float*rotorFL,float*rotorRR,float*rotorRL){
	*rotorFL=thr-roll-pitch+yaw; *rotorFR=thr+roll-pitch-yaw;
	*rotorRL=thr-roll+pitch-yaw; *rotorRR=thr+roll+pitch+yaw;
	float max_out;
	float max_front;
	float max_rear;
	if(*rotorFL>*rotorFR){
		max_front=*rotorFL;
	}else{
		max_front=*rotorFR;
	}
	if(*rotorRL>*rotorRR){
		max_rear=*rotorRL;
	}else{
		max_rear=*rotorRR;
	}
	if(max_rear>max_front){
		max_out=max_rear;
	}else{
		max_out=max_front;
	}
	if(max_out>1.0f){
		float over=max_out-1.0f;
		*rotorFL-=over;
		*rotorFR-=over;
		*rotorRL-=over;
		*rotorRR-=over;
	}
	if(*rotorFL<0){
		*rotorFL=0;
	}
	if(*rotorFR<0){
		*rotorFR=0;
	}
	if(*rotorRL<0){
		*rotorRL=0;
	}
	if(*rotorRR<0){
		*rotorRR=0;
	}
}

void ParseExternalRC(SerialIO*RCtoSerial,int&ch1,int&ch2){
	char*inputstr=RCtoSerial->GetReceivedString();
	sscanf(inputstr,"%d,%d",&ch1,&ch2);
}
/**
 * @}
 */
