/*
 * syscalls.h
 *
 *  Created on: 2015/08/28
 *      Author: �@��Y
 */

#ifndef SYSCALLS_H_
#define SYSCALLS_H_
#ifdef __cplusplus
extern "C"{
#endif
int _read (int file, char *ptr, int len);
int _write(int file, char *ptr, int len);
int _open(char *path, int flags, ...);

#ifdef __cplusplus
}
#endif
#endif /* SYSCALLS_H_ */
