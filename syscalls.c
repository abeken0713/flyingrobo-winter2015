/**
*****************************************************************************
**
**  File        : syscalls.c
**
**  Abstract    : Atollic TrueSTUDIO Minimal System calls file
**
** 		          For more information about which c-functions
**                need which of these lowlevel functions
**                please consult the Newlib libc-manual
**
**  Environment : Atollic TrueSTUDIO
**
**  Distribution: The file is distributed is,without any warranty
**                of any kind.
**
**  (c)Copyright Atollic AB.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the Atollic TrueSTUDIO(R) Pro toolchain.
**
*****************************************************************************
*/

/* Includes */
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/fcntl.h>
#include "HAL/StreamIOManager.h"
#include "syscalls.h"
/* Variables */
//#undef errno
extern int errno;
extern int __io_putchar(int ch) __attribute__((weak));
extern int __io_getchar(void) __attribute__((weak));

register char * stack_ptr asm("sp");

char *__env[1] = { 0 };
char **environ = __env;


/* Functions */
void initialise_monitor_handles()
{
}

int _getpid(void)
{
	return 1;
}

int _kill(int pid, int sig)
{
	errno = EINVAL;
	return -1;
}

void _exit (int status)
{
	_kill(status, -1);
	while (1) {}		/* Make sure we hang here */
}

int _read (int file, char *ptr, int len)
{
	int DataIdx=0;
	StreamIOinfo_t*info=StreamIO_retrival_by_Fd(file);
	if(info!=0){
		if(info->IsNotEmptyfunc!=0&&info->Readfunction!=0){
			for (DataIdx = 0; DataIdx < len; DataIdx++)
			{
				if(info->IsNotEmptyfunc()==0&&((info->flag&O_NONBLOCK)||(info->flag&O_BINARY))){//�󂩃m���u���N
					break;
				}
				*ptr = info->Readfunction();
				if(!(info->flag&O_BINARY)){
					info->Writefunction(*ptr);
				}
				if(*ptr=='\r'&&((info->flag&O_BINARY)==0)){
					*ptr='\n';
					DataIdx++;
					break;
				}
				ptr++;
			}
		}
	}
return DataIdx;
}

int _write(int file, char *ptr, int len)
{
	int DataIdx;
	StreamIOinfo_t*info=StreamIO_retrival_by_Fd(file);
	if(info==0){
		return 0;
	}
	if(info->Writefunction==0){
		return 0;
	}
	for (DataIdx = 0; DataIdx < len; DataIdx++)
	{
	  // __io_putchar( *ptr++ );
	   info->Writefunction(*ptr);
	   ptr++;
	}
	return DataIdx;
}

caddr_t _sbrk(int incr)
{
	extern char end asm("end");
	static char *heap_end;
	char *prev_heap_end;

	if (heap_end == 0)
		heap_end = &end;

	prev_heap_end = heap_end;
	if (heap_end + incr > stack_ptr)
	{
//		write(1, "Heap and stack collision\n", 25);
		//printf("Heap and stack collision\n");
//		abort();
		errno = ENOMEM;
		return (caddr_t) -1;
	}

	heap_end += incr;

	return (caddr_t) prev_heap_end;
}

int _close(int file)
{
	return -1;
}


int _fstat(int file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _isatty(int file)
{
	return 1;
}

int _lseek(int file, int ptr, int dir)
{
	return 0;
}

int _open(char *path, int flags, ...)
{
	/* Pretend like we always fail
	return -1;*/
	StreamIOinfo_t*info=StreamIO_retrival_by_Name(path);
	if(info==0){
		return -1;
	}else{
		info->flag|=flags;
		return info->fd;
	}
}

int _wait(int *status)
{
	errno = ECHILD;
	return -1;
}

int _unlink(char *name)
{
	errno = ENOENT;
	return -1;
}

int _times(struct tms *buf)
{
	return -1;
}

int _stat(char *file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _link(char *old, char *new)
{
	errno = EMLINK;
	return -1;
}

int _fork(void)
{
	errno = EAGAIN;
	return -1;
}

int _execve(char *name, char **argv, char **env)
{
	errno = ENOMEM;
	return -1;
}
void *__dso_handle=0;

