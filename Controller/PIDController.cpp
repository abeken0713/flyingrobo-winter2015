/*
 * PIDController.cpp
 *
 *  Created on: 2014/12/31
 *      Author: ���@�@��Y
 */

#include "PIDController.h"

PIDController::PIDController() {
	// TODO Auto-generated constructor stub

}
PIDController::PIDController(float Kp,float Ki,float Kd,float Antiwindupgain,float Outlimit,bool AntiwindupEnable){
	this->Kp=Kp;
	this->Ki=Ki;
	this->Kd=Kd;
	this->integral=0;
	this->prev_err=0;
	this->AntiWindupEnable=AntiwindupEnable;
	this->AntiWindupGain=Antiwindupgain;
	this->Outputlimit=Outlimit;
}
float PIDController::Execute(float Error){
	integral+=Error;
	float diff=Error-prev_err;
	prev_err=Error;
	float desired_output= Kp*Error+Ki*integral+Kd*diff;
	if(this->AntiWindupEnable){
		if(desired_output>this->Outputlimit){
			integral=integral-AntiWindupGain*(desired_output-Outputlimit);
			desired_output=Outputlimit;
		}else if(-desired_output<(-this->Outputlimit)){
			integral=integral-AntiWindupGain*(desired_output+Outputlimit);
			desired_output=-Outputlimit;
		}
	}
	return desired_output;
}
void PIDController::ParameterUpdate(float Kp,float Ki,float Kd){
	this->Kd=Kd;
	this->Kp=Kp;
	this->Ki=Ki;
}
void PIDController::Reset(void){
	this->integral=0;
	this->prev_err=0;
}
PIDController::~PIDController() {
	// TODO Auto-generated destructor stub
}

