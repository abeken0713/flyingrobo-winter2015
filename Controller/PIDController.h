/*
 * PIDController.h
 *
 *  Created on: 2014/12/31
 *      Author: ���@�@��Y
 */

#ifndef PIDCONTROLLER_H_
#define PIDCONTROLLER_H_

class PIDController {
private:
	float prev_err;
	float integral;
	float Kp;
	float Ki;
	float Kd;
	float AntiWindupGain;
	bool AntiWindupEnable;
	float Outputlimit;
public:
	PIDController();
	PIDController(float Kp,float Ki,float Kd,float Antiwindupgain=0.0f,float Outlimit=0.0f,bool AntiwindupEnable=false);
	float Execute(float Error);
	void ParameterUpdate(float Kp,float Ki,float Kd);
	void Reset(void);
	virtual ~PIDController();
};

#endif /* PIDCONTROLLER_H_ */
