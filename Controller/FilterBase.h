/*
 * FilterBase.h
 *
 *  Created on: 2015/01/03
 *      Author: ���@�@��Y
 */

#ifndef FILTERBASE_H_
#define FILTERBASE_H_

class FilterBase {
public:
	FilterBase();
	virtual float Execute(float inputval);
	virtual ~FilterBase();
};

#endif /* FILTERBASE_H_ */
