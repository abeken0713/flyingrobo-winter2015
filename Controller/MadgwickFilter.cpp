/*
 * MadgwickFilter.cpp
 *
 *  Created on: 2015/08/15
 *      Author: 岩滝　宗一郎
 */

#include "MadgwickFilter.h"

MadgwickFilter::MadgwickFilter(float dt_,float beta_):Attitude(1.0f,0,0,0),AccOnEarth(0,0,0,1.0f){
	dt=dt_;
	beta=beta_;
}
Quaternion MadgwickFilter::GetAttitude(void){
	return this->Attitude;
}
void MadgwickFilter::Update(const Vector&omega,const Vector&acc){
	Quaternion prevAtti(this->Attitude);
	//ヤコビアン(J)を求める
	Matrix Jacobian(4,4);
	//バグ　転置必要，行列サイズ不正
	float q0=prevAtti.GetElem(0),q1=prevAtti.GetElem(1),q2=prevAtti.GetElem(2),q3=prevAtti.GetElem(3);
	Jacobian.At(0,0,0.0f);Jacobian.At(0,1,0.0f);Jacobian.At(0,2,0.0f);Jacobian.At(0,3,0.0f);
	Jacobian.At(1,0,-2.0f*q2); Jacobian.At(1,1,2.0f*q3); Jacobian.At(1,2,-2.0f*q0); Jacobian.At(1,3,2.0f*q1);
	Jacobian.At(2,0,2.0f*q1); Jacobian.At(2,1,2.0f*q0); Jacobian.At(2,2,2.0f*q3); Jacobian.At(2,3,2.0f*q2);
	Jacobian.At(3,0,2.0f*q0);Jacobian.At(3,1,-2.0f*q1);Jacobian.At(3,2,-2.0f*q2);Jacobian.At(3,3,2.0f*q3);
	//修正量 nabraF=JFを求める
	Quaternion Accq(acc);
	Vector NabraFVec=Jacobian.Transposed()*(prevAtti.Conj()*AccOnEarth*prevAtti-Accq).ToVector4d();
	NabraFVec.Normalize();
	NabraFVec=NabraFVec*beta;
	//角速度による姿勢の変化量を求める
	Vector deltaAttitude=Quaternion::FindOmegaMatrix(omega)*prevAtti.ToVector4d()*0.5f*dt;
	//修正を行う
	deltaAttitude=deltaAttitude-NabraFVec;
	//積算する
	Vector NewAttitude=prevAtti.ToVector4d()+deltaAttitude;
	//正規化する
	NewAttitude.Normalize();
	Attitude.FromVector(NewAttitude);
}
void MadgwickFilter::Reset(void){
	Vector initial(4);
	initial.At(0,1.0f);initial.At(1,0.0f);initial.At(2,0.0f);initial.At(3,0.0f);
	Attitude.FromVector(initial);
}
MadgwickFilter::~MadgwickFilter(){

}
