/*
 * FIRFilter.h
 *
 *  Created on: 2015/01/03
 *      Author: ���@�@��Y
 */

#ifndef FIRFILTER_H_
#define FIRFILTER_H_

#include "FilterBase.h"

class FIRFilter: public FilterBase {
private:
	float*coefficent;
	float*delayedvalue;
	int tap;
public:
	FIRFilter(float coeff[],int tap);
	float Execute(float value);
	virtual ~FIRFilter();
};

#endif /* FIRFILTER_H_ */
