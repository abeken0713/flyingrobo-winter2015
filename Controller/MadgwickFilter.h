/*
 * MadgwickFilter.h
 *
 *  Created on: 2015/08/15
 *      Author: ���@�@��Y
 */

#ifndef MADGWICKFILTER_H_
#define MADGWICKFILTER_H_

#include "LinearAlgebra/Vector.h"
#include "LinearAlgebra/Matrix.h"
#include "LinearAlgebra/Quaternion.h"

class MadgwickFilter{
private:
	Quaternion Attitude;//�p��
	Quaternion AccOnEarth;//�d�͉����x
	float beta;
	float dt;
public:
	MadgwickFilter(float dt,float beta);
	Quaternion GetAttitude(void);
	void Update(const Vector&omega,const Vector&acc);
	void Reset(void);
	~MadgwickFilter();
};



#endif /* MADGWICKFILTER_H_ */
